##################################################
# Kurssi: AT00BT78-3005 - Oliot ja tietokannat
# Ohjelmanimi: LT-Taso-2
# Tekijä: Ferhat Arslan
#
# Vakuutan, että tämä ohjelma on minun tekemä.
# Työhön olen käyttänyt seuraavia lähteitä, sekä
# saanut apua seuraavilta henkilöiltä:
# - Lähde = Tämän kurssin lähteet ovat moodlesta.
# - Henkilö = Ei kukaan.
##################################################

import sqlite3

class Kayttaja:
    nimi: str
    tiimi: str
    rooli: str
    
    def __init__(self, nimi: str, tiimi: str, rooli: str) -> None:
        self.nimi = nimi
        self.tiimi = tiimi
        self.rooli = rooli
        return None

class Tavara:
    nimike: str
    arvo: float

    def __init__(self, nimike: str, arvo: float) -> None:
        self.nimike = nimike
        self.arvo = arvo
        return None

class Tietokanta:
    # 3.11 versio tukee muotoa list[Tavara]
    # Saattaa olla ongelmallinen CodeGradessa.
    tavarat: list[Tavara] = [
        Tavara('Kahvimuki', 1), Tavara('Haarniska', 2304), Tavara('Kala', 14.45), Tavara('Vasara', 19.99),
        Tavara('Tulitikut', 0.25), Tavara('Hammasharja', 0.65), Tavara('Miekka', 3680), Tavara('Taideteos', 1840)]

    tieto: list[Kayttaja] = [
        Kayttaja('John','Rakettiryhmä','Pelaaja'), Kayttaja('Eric','Vedenalla','Pelaaja')
    ]

    def __init__(self) -> None:
        self.polku = {"FILEPATH": "kanta.db"}
        self.yhteys = sqlite3.connect("kanta.db") # Kuinka dictionaryä käytetään
        self.kursori = self.yhteys.cursor()
        self.kayttajatTaulukko()
        self.tavaratTaulukko()
        return None

    def luoYhteys(self) -> None:
        try:
            if self.yhteys == None:
                self.yhteys = sqlite3.connect(self.polku)
        except Exception as virhe:
            print("Virhe tietokantayhteyden muodostamisessa.")
            print(virhe)
        return None

    def kayttajatTaulukko(self) -> None:
        self.kursori.execute("""CREATE TABLE IF NOT EXISTS Kayttajat(
            nimi TEXT PRIMARY KEY,
            tiimi TEXT NOT NULL,
            rooli TEXT NOT NULL
        );""")
        return None

    def tavaratTaulukko(self) -> None:
        self.kursori.execute("""CREATE TABLE IF NOT EXISTS Tavarat(
            nimike TEXT PRIMARY KEY,
            arvo INTEGER NOT NULL
        );""")
        return None

    def muodostaTavarat(self) -> None:
        _tavarat = [(tavara.nimike, tavara.arvo,) for tavara in self.tavarat]
        self.kursori.executemany("""INSERT OR IGNORE INTO Tavarat VALUES(?,?);""", _tavarat)
        self.yhteys.commit()
        return None

    def muodostaKayttajat(self) -> None:
        _tieto = [(tieto.nimi, tieto.tiimi, tieto.rooli,) for tieto in self.tieto]
        self.kursori.executemany("""INSERT OR IGNORE INTO Kayttajat VALUES(?,?,?);""", _tieto)
        self.yhteys.commit()
        return None

    def haeTiedot(self) -> list[tuple]: # Saattaa olla ongelmallinen CodeGradessa.
        self.syote = input("Syötä tiimin nimi: ")
        self.tiimi = str(self.syote)
        self.kursori.execute("SELECT tiimi FROM Kayttajat WHERE tiimi = (?)", self.tiimi)
        self.tiimi = self.kursori.fetchall()
        for kayttaja in self.tiimi:
            print(kayttaja)
        return None

    def poistaTieto(self, nimi: str) -> None:
        poistaKysely = f"DELETE FROM Kayttajat WHERE nimi = {nimi}"
        self.kursori.execute(poistaKysely)
        self.yhteys.commit()
        self.yhteys.close()
        print(f"Käyttäjä {nimi} poistettu.")
        return None

    def listaaKayttajat(self) -> None:
        self.kursori.execute("SELECT * FROM Kayttajat;")
        print(self.kursori.fetchall())
        self.yhteys.commit()
        return None

class Valikko:
    taso: int # ylä vai alavalikko? sisennyksen määrä?
    valinta: int = -1
    vaihtoehdot: list[str]

    def __init__(self, vaihtoehdot: list[str], taso: int) -> None:
        self.vaihtoehdot = vaihtoehdot
        self.taso = taso
        return None

    def kysy(self):
        return None

    def tulosta(self, sisalto: str):
        if self.taso == 1:
            print(sisalto)
        elif self.taso == 0:
            print(sisalto)
        elif self.taso == 2:
            print(sisalto)
        return None

    def suorita(self) -> None:
        try:
            for i in range(0,len(self.vaihtoehdot)):
                self.tulosta(str(i + 1) + " - " + self.vaihtoehdot[i])
            if self.taso == 0:
                self.tulosta("0 - Lopeta ohjelma")
            elif self.taso == 1 or 2:
                self.tulosta("0 - Palaa edelliseen valikkoon")
            syote = input("Valintasi: ")
            self.valinta = int(syote)
        except:
            self.valinta = -1
        return None

# KayttajaValikko - alavalikko
class KayttajaValikko(Valikko):
    def __init__(self) -> None:
        super().__init__(["Lisää käyttäjä", "Listaa käyttäjät",
        "Etsi kaikki tiimiin kuuluvat käyttäjät", "Muokkaa käyttäjän tiimiä", "Poista käyttäjä"], 1)
        self.tietokanta = Tietokanta()
        return None
    
    def menu(self):
        print("Alavalikko: ")
        while True:
            self.suorita()
            if self.valinta == 1:
                print("Lisää käyttäjä")
            elif self.valinta == 2:
                self.tietokanta.listaaKayttajat()
            elif self.valinta == 3:
                self.tietokanta.haeTiedot()
            elif self.valinta == 4:
                print("Muokkaa käyttäjän tiimiä")
            elif self.valinta == 5:
                self.nimi = input("Syötä poistettavan käyttäjän nimi: ")
                _valinta = input("Oletko aivan varma, että haluat poistaa käyttäjän ",{}," (K/E): ".format(self.nimi))
                if _valinta == 'K':
                    self.tietokanta.poistaTieto(nimi="")
                elif _valinta == 'E':
                    print("Et halunnut poistaa käyttäjää")
                else:
                    print("Väärä valinta, yritä uudelleen")
            else:
                break
        return None

# TavaraValikko - alavalikko
class TavaraValikko(Valikko):
    def __init__(self) -> None:
        super().__init__(["Näytä tavarat", "Hinnoittele tavara uudelleen"], 2)
        return None
    
    def menu(self):
        print("Alavalikko: ")
        while True:
            self.suorita()
            if self.valinta == 1:
                print("Näytä tavarat") # Viesti valikosta tietokantaan
            elif self.valinta == 2:
                print("Hinnoittele tavara uudelleen")
            else:
                break
        return None

class Main(Valikko):
    print("Tervetuloa pelisysteemiin.\n")
    
    def __init__(self) -> None:
        super().__init__(["Käyttäjät", "Tavarat"],0)
        self.tietokanta = Tietokanta()
        self.tietokanta.muodostaTavarat()
        self.tietokanta.muodostaKayttajat()
        self.kayttajavalikko = KayttajaValikko()
        self.tavaravalikko = TavaraValikko()
        self.menu()
        return None
    
    def menu(self) -> None: # suoritaValinta
        while True:
            print("Pääohjelma: ")
            self.suorita()
            if self.valinta == 1: # Käyttäjävalikko
                self.kayttajavalikko.menu()
            elif self.valinta == 2: # Tavaravalikko
                self.tavaravalikko.menu()
            elif self.valinta == 0:
                print("Ohjelma päättyy.")
                break
            else:
                print("Tuntematon valinta, yritä uudelleen.")
        print()
        print("Ensikertaan!")
        return None

if __name__ == "__main__":
    peli = Main()
